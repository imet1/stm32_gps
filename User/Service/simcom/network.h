/*
 * network.h
 *
 *  Created on: Jan 29, 2019
 *      Author: dongnx
 */

#ifndef _NETWORK_H_
#define _NETWORK_H_

#define NETWORK_MODE_AUTO			2
#define NETWORK_MODE_GSM			13
#define NETWORK_MODE_WCDMA			14

#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "simcom.h"

typedef struct network_t network_t;

typedef struct network_p {
	int(*connect)(network_t*, const char*, const char*, const char*);
	int(*disconnect)(network_t*);
	int(*get_signal)(network_t*);
} network_p;

struct network_t {
	const network_p* proc;
	simcom_t* simcom;
	bool connected;
	char* operator_name;
	char* ip;
	int provider;
	int signal;
    int error;
};

network_t* network_create(simcom_t* simcom);
void network_dispose(network_t* object);
int network_disconnect(network_t* object);
int network_connect(network_t* object, const char* apn, const char* user, const char* pwd);
int network_get_signal(network_t* object);

const char* network_get_operator_name(network_t* object);
const char* network_get_ip(network_t* object);

int network_get_registration_status(network_t *object);
int network_set_gprs_status(network_t *object, bool state);
int network_get_gprs_status(network_t *object);
int network_get_pd_status(network_t *object);
int network_get_pdp_context_status(network_t *object, int cid);
int network_get_sock_state(network_t *object);
int network_set_APN(network_t *object, char *apn, char *user, char *pwd);
int network_bringup_connection(network_t *object);

int network_dns_get_ip_by_hostname(network_t* object, uint32_t* ip_addr, const char* host_name);
int network_dns_get_hostname_by_ip(network_t* object, uint32_t ip_addr, const char* host_name);


#endif /* _NETWORK_H_ */
