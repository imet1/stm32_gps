/*
 * mqtt.h
 *
 *  Created on: Jan 13, 2023
 *      Author: manht
 */

#ifndef SERVICE_MQTT_MQTT_H_
#define SERVICE_MQTT_MQTT_H_

#include "main.h"
#include "socket.h"
#include "core_mqtt.h"

typedef struct MQTT_t MQTT_t;

typedef struct MQTT_p {
    MQTTStatus_t (*start)(MQTT_t *client);
    int (*publish)(MQTT_t *client, const char *topic, const char *data, int len, int qos, int retain);
} MQTT_p;

struct MQTT_t {
    const MQTT_p* proc;
    MQTTContext_t* context;
    MQTTConnectInfo_t* info;
};
typedef enum {
    MQTT_EVENT_ANY = -1,
    MQTT_EVENT_ERROR = 0,
    MQTT_EVENT_CONNECTED,
    MQTT_EVENT_DISCONNECTED,
    MQTT_EVENT_SUBSCRIBED,
    MQTT_EVENT_UNSUBSCRIBED,
    MQTT_EVENT_PUBLISHED,
    MQTT_EVENT_DATA,
    MQTT_EVENT_BEFORE_CONNECT,
    MQTT_EVENT_DELETED,

} MQTTEventID_t;
typedef struct {
    MQTTEventID_t event_id;       /*!< MQTT event type */
    MQTT_t* client;             /*!< MQTT client handle for this event */
    char *data;                         /*!< Data associated with this event */
    int data_len;                       /*!< Length of the data for this event */
    char *topic;                        /*!< Topic associated with this event */
    int topic_len;                      /*!< Length of the topic for this event associated with this event */
    int msg_id;                         /*!< MQTT messaged id of message */
    int session_present;                /*!< MQTT session_present flag for connection event */
    int error_code;                     /*!< esp-mqtt error handle including esp-tls errors as well as internal mqtt errors */
    bool retain;                        /*!< Retained flag of the message associated with this event */
    int topic_idx;
    int qos;
} MQTTEvent_t;
typedef void(*eventCallback)(MQTTEvent_t *event);
MQTT_t* MQTTInit(TransportInterface_t * interface);
MQTTStatus_t MQTTStart(MQTT_t* client, MQTTConnectInfo_t* info);
MQTTStatus_t MQTTSubcribe(MQTT_t* client, const MQTTSubscribeInfo_t *pSubscriptionList, size_t subscriptionCount);
MQTTStatus_t MQTTPublish(MQTT_t* client, const char *topic, const char * payload, int len, int qos, int retain);
void MQTTRegisterCallback(eventCallback cb);
void MQTTProcess(MQTT_t* client);

#endif /* SERVICE_MQTT_MQTT_H_ */
