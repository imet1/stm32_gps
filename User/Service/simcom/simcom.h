/*
 * fifo.h
 *
 *  Created on: Nov 29, 2021
 *      Author: manht
 */

#ifndef _SIMCOM_H_
#define _SIMCOM_H_

#include "stdint.h"
#include "stdbool.h"
#include "string.h"
#include "stdlib.h"

#include "atc.h"

#define SIM_DEBUG		0

#define SIM_HW_RESET	0	// hardware reset
#define SIM_AT_RESET	1	// AT command reset
#define SIM_PW_RESET	2	// power key reset

#define SIM_ACTIVE      1
#define SIM_DEACTIVE    (!SIM_ACTIVE)
#define SIM_PWKEY_MODE  GPIO_MODE_OUTPUT_PP

#define AT_OK			"\x1B[33mOK"
#define AT_ERROR		"\x1B[31mERROR"
#define FUNC_SUCCESS	"\x1B[33mSUCCESS"
#define FUNC_FAIL		"\x1B[31mFAIL"

#define AT_RETRY		5
typedef struct simcom_t simcom_t;

typedef struct simcom_p {
	void(*dispose)(simcom_t*);
	int(*reset)(simcom_t*, int);
	int(*set_power)(simcom_t*, bool);
	int(*flight_mode)(simcom_t*, bool);
	int(*send_AT)(simcom_t*, const char*);
	int(*send_data)(simcom_t*, uint8_t*, int);
	int(*receive)(simcom_t* object, uint8_t* response, int size, int timeout);
	int(*receive_ex)(simcom_t* object, const char* expect, int timeout, uint8_t* buffer, int size);
	void(*process)(simcom_t* object);
} simcom_p;

struct simcom_t {
	const simcom_p* proc;
	embedded_at_t* serial;
	bool pending;
	int error_count;
	char* serial_number;
    char* fw_version;
    char* cgns_version;
	char* model;
	char* vendor;
	int sleep_mode;
	uint32_t id;
};
typedef enum {
	SC_READY_EVT,
	SC_NO_SIMCARD_EVT,
	SC_SOCK_DATA_EVT,
	SC_SOCK_DATARDY_EVT,
	SC_SOCK_CLOSE_EVT,
} sc_evt_e;
typedef struct sc_evt_param_t
{
	uint8_t data[2048];
	int size;
	int sid;	//session_id
} sc_evt_param_t;
simcom_t* simcom_create(void);
void simcom_dispose(simcom_t* object);
int simcom_reset(simcom_t* object, int mode);
int simcom_set_power(simcom_t* object, bool active);
int simcom_flight_mode(simcom_t* object, bool active);
int simcom_entry_sleep(simcom_t* object);
int simcom_exit_sleep(simcom_t* object);
bool simcom_wait_pwron(simcom_t * object, uint32_t timeout);
bool simcom_wait_ready(simcom_t *object, uint32_t timeout);
int simcom_get_csq(simcom_t *object);

int simcom_get_battery(simcom_t* object);
const char* simcom_get_manufacturer_id(simcom_t* object);
const char* simcom_get_model_id(simcom_t* object);
const char* simcom_get_revsion_id(simcom_t* object);
const char* simcom_get_fw_version(simcom_t* object);
const char* simcom_get_cgns_version(simcom_t* object);
const char* simcom_get_serial_number(simcom_t* object);
void osDelay(uint32_t Tick);
uint32_t simcom_get_id(simcom_t* object);
uint32_t eat_get_current_time(void);
int simcom_send_AT(simcom_t *object, const char *message);
int simcom_send_data(simcom_t* object, uint8_t* data, int size);
int simcom_receive(simcom_t* object, uint8_t* response, int size, int timeout);
int simcom_receive_ex(simcom_t* object, const char* expect, int timeout, uint8_t* buffer, int size);
void simcom_process(simcom_t * simcom);
void simcom_register_cb(void(*callback)(simcom_t * simcom, sc_evt_e event_id, void *param));

#endif /* _SIMCOM_H_ */
