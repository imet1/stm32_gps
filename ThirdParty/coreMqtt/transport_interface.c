#include "transport_interface.h"
#include "log.h"

#define SOCK_READ_TIMEOUT  10000
static const char *TAG = "transport_if";
static TransportInterface_t transport_if_obj[MAX_SOCKET_INDEX]= {0};
static NetworkContext_t  network_ctx_obj[MAX_SOCKET_INDEX] ={0};
static int32_t transport_if_send(NetworkContext_t * pNetworkContext,
                                       const void * pBuffer,
                                       size_t bytesToSend );
static int32_t transport_if_read( NetworkContext_t * pNetworkContext,
                                       void * pBuffer,
                                       size_t bytesToRecv);                                     
NetworkContext_t * network_ctx_create(socket_t * socket)
{
    NetworkContext_t * obj = NULL;
    if (socket == NULL)
    	return NULL;
    int index = socket->index;
    obj = &network_ctx_obj[index];
    obj->tcpSocketCtx = socket;
    return obj;
}
TransportInterface_t * transport_if_create(NetworkContext_t * network_cxt)
{
    if (network_cxt == NULL) return NULL;
    TransportInterface_t * obj = NULL;
    int index = network_cxt->tcpSocketCtx->index;
    obj = &transport_if_obj[index];
    obj->pNetworkContext = network_cxt;
    obj->send = transport_if_send;
    obj->recv = transport_if_read;
    return obj;
}
static int32_t transport_if_send(NetworkContext_t * pNetworkContext,
                                       const void * pBuffer,
                                       size_t bytesToSend )
{
    int32_t bytesSent = 0;
    socket_t * sock = pNetworkContext->tcpSocketCtx;
    bytesSent = sock->proc->send(sock,(uint8_t*)pBuffer, (int) bytesToSend);
    return bytesSent;

}
static int32_t transport_if_read( NetworkContext_t * pNetworkContext,
                                       void * pBuffer,
                                       size_t bytesToRecv)
{
    socket_t * sock = pNetworkContext->tcpSocketCtx;
    if (sock == NULL)
    	return 0;
    return sock->proc->read(sock, (uint8_t*)pBuffer, (int) (bytesToRecv), SOCK_READ_TIMEOUT);
}
