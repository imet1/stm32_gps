/*
 * app_simcom.c
 *
 *  Created on: Jan 11, 2023
 *      Author: manht
 */
#include "main.h"
#include "atc.h"
#include "log.h"
#include "stimer.h"
#include "simcom.h"
#include "network.h"
#include "socket.h"
#include "fifo.h"

#include "FreeRTOS.h"
#include "task.h"

static const char *TAG = "app_simcom";
extern void AppMqttInit(void);
fifo_t *sc_fifo = NULL;
uint8_t sc_buffer[2048] = {0};

fifo_t *sc_tx = NULL;
fifo_t *sc_rx = NULL;
uint8_t tx_buf[256] = {0};
uint8_t rx_buf[2048] = {0};

uint8_t sc_data = 0;
embedded_at_t * sc_atc = NULL;
simcom_t *sc = NULL;
network_t *net = NULL;
socket_t *sock = NULL;

int sc_uart_write(uint8_t *data, int size);
int sc_uart_read(uint8_t *data, int size);
int sc_uart_readblock(uint8_t *data, int size);
void sc_uart_flush(void);

int sc_uart_write(uint8_t *data, int size){
	HAL_UART_Transmit(&huart3, data, size, 1000);
	return size;
}
int sc_uart_read(uint8_t *data, int size){
	return (int) FifoGets(sc_rx, data, size);
}
int sc_uart_readblock(uint8_t *data, int size){
	return (int) FifoGets(sc_rx, data, size);
}
void sc_uart_flush(void){
	FifoReset(sc_rx);
	memset(sc_rx->buf, 0, 2048);
}
int sc_uart_get_data_rdy(void)
{
	return FifoAvailable(sc_rx);
}

void SimcomEventCallback(simcom_t * simcom, sc_evt_e event_id, void *param)
{
//	if (event_id == SC_SOCK_DATA_EVT)
//	{
//		sc_evt_param_t *ptr = (sc_evt_param_t*)param;
//		if (ptr->size > 0 && sock->fifo != NULL)
//			FifoPuts(sock->fifo, (uint8_t*)&ptr->data, ptr->size);
//	}
	if (event_id == SC_SOCK_DATARDY_EVT)
	{
		uint8_t Data[1024] = {0};
		int cnlen = -1;
		int reqlen = 0;
		char cmd[32] = {0};
		int idx = 0;
		while (cnlen != 0)
		{
			do {
				sprintf(cmd,"+CIPRXGET=2,1000");
				if (simcom->proc->send_AT(simcom, cmd) < 0)
					break;
				int msg_size = simcom->proc->receive_ex(simcom, "OK\r\n", 5000,  Data, 1024);
				if (msg_size < 0)
					break;
				char *ptr = (char*) &Data[34];
				char tmp[5] = {0};
				idx = 0;
				while ( *ptr != ',') {
					tmp[idx++] = *ptr;
					ptr++;
				}
				ptr++;
				idx = 0;
				reqlen = atoi(tmp);
				memset(tmp, 0, 5);
				while ( *ptr != '\r') {
					tmp[idx++] = *ptr;
				  ptr++;
				}
				cnlen = atoi(tmp);
				ptr = strstr((const char*)&Data[34], "\r\n");
				if (ptr != NULL)
				{
					FifoPuts(sock->fifo, (uint8_t*)&ptr[2], reqlen);
				}
//				Logi("socket: %d bytes", reqlen);
			} while (0);
			osDelay(100);
		}

	}
}
static void AppSimcomInit()
{
	Logi("Waiting GSM Module Power on...");
    if (!simcom_wait_pwron(sc, 60000))
    {
        Loge( "GSM Module can't power on.");
    }
    do
    {
        simcom_get_manufacturer_id(sc);
        simcom_get_model_id(sc);
        simcom_get_fw_version(sc);
        simcom_get_serial_number(sc);
        network_get_operator_name(net);
        if ((sc->model != NULL) && (sc->vendor != NULL) && (sc->fw_version != NULL)
        		&& (sc->serial_number != NULL) && (net->operator_name != NULL))
            break;
        vTaskDelay(1000);
    } while (1);
    Logi( "  MODULE  : %s", sc->model);
    Logi( "  VENDOR  : %s", sc->vendor);
    Logi( "  IMEI    : %s", sc->serial_number);
    Logi( "  FW      : %s", sc->fw_version);
    Logi( "  NETWORK : %s", net->operator_name);
    simcom_register_cb(SimcomEventCallback);
    int ret = -1;
    do {
    	ret = network_get_registration_status(net);
    	vTaskDelay(1000);
    } while (ret != 1);
    Logi( "%s register network %s", sc->model, FUNC_SUCCESS);
	sc->proc->send_AT(sc, "+CIPRXGET=1");
	uint8_t buffer[100] = {0};
	sc->proc->receive_ex(sc, "OK\r\n", 2000, buffer, 100);
	sc->proc->send_AT(sc, "+CIPQSEND=1");
	sc->proc->receive_ex(sc, "OK\r\n", 2000, buffer, 100);
	network_set_gprs_status(net, 1);
	network_set_APN(net, "m-wap", "mms", "mms");
	network_bringup_connection(net);
	network_get_ip(net);
}
void AppSimcomLoop(void *param)
{
	TickType_t xLastWakeTime = xTaskGetTickCount();
	const TickType_t xFrequency = 100;
	while(1)
	{
		sc->proc->process(sc);
		vTaskDelayUntil(&xLastWakeTime, xFrequency);
	}
}
void AppSimcomStart(void)
{
	sc_fifo = FifoInit(sc_buffer, 2048);
	sc_tx = FifoInit(tx_buf, 256);
	sc_rx = FifoInit(rx_buf, 2048);
	register_eat_write_event(sc_uart_write);
	register_eat_read_event(sc_uart_read);
	register_eat_read_block_event(sc_uart_readblock);
	register_eat_flush_event(sc_uart_flush);
	register_eat_get_data_rdy_event(sc_uart_get_data_rdy);
	HAL_GPIO_WritePin(PWR_GSM_GPIO_Port, PWR_GSM_Pin, GPIO_PIN_SET);
	HAL_Delay(1000);
	HAL_GPIO_WritePin(PWR_GSM_GPIO_Port, PWR_GSM_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(PWRKEY_GPIO_Port, PWRKEY_Pin, GPIO_PIN_RESET);
	HAL_Delay(1500);
	HAL_GPIO_WritePin(PWRKEY_GPIO_Port, PWRKEY_Pin, GPIO_PIN_SET);
	sc = simcom_create();
	net = network_create(sc);
	if (sc == NULL || net == NULL)
	{
		Logi("create simcom failed");
		return;
	}
	HAL_UART_Receive_IT(&huart3, &sc_data, 1);
	AppSimcomInit();
	xTaskCreate(AppSimcomLoop, "AppSimcomLoop", 2000, NULL, configMAX_PRIORITIES - 2, NULL);
}

