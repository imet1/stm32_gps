/*
 * gps_type.h
 *
 *  Created on: Mar 9, 2023
 *      Author: manht
 */

#ifndef APP_GPS_TYPE_H_
#define APP_GPS_TYPE_H_

#include "stdint.h"
#include "stdbool.h"
typedef struct {
	uint32_t begin;
	uint8_t data[256];
	int size;
} gps_data_t;

extern bool is_end;
extern gps_data_t data;
#endif /* APP_GPS_TYPE_H_ */
