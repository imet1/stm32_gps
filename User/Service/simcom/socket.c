/*
 * socket.c
 *
 *  Created on: Aug 24, 2020
 *      Author: ManhTH
 */
#include "main.h"
#include "log.h"
#include "simcom.h"
#include "socket.h"

static const char *TAG = "socket";
static socket_p socket_proc = {
		socket_open,
		socket_send,
		socket_read,
		socket_close
};

static socket_t socket_object[MAX_SOCKET_INDEX] = {0};

socket_t* socket_create_tcp(simcom_t* simcom, int index) {
	socket_t* socket = NULL;
	uint8_t *fifo = NULL;
    do {
        if (index >= MAX_SOCKET_INDEX)
            break;
        socket = &socket_object[index];
        fifo = (uint8_t*)malloc(2048);
		if (fifo == NULL)
		{
			Loge("can't allocated ring buffer");
			socket = NULL;
			break;
		}
		socket->fifo = FifoInit((uint8_t*)fifo, 2048);
		if (socket->fifo == NULL)
		{
			socket = NULL;
			break;
		}
        socket->proc = &socket_proc;
        socket->simcom = simcom;
        socket->index = index;
        socket->connected = false;
        socket->error = 0;
    } while (0);
    Logi("%s create socket %s", simcom->model, (socket->error == 0) ? FUNC_SUCCESS : FUNC_FAIL);
	return socket;
}

socket_t* socket_create_udp(simcom_t* simcom, int index, uint32_t ip, const char* host, int port) {
	return NULL;
}
int socket_open(socket_t* sock, const char* host, int port)
{
	int ret = -1;
	if (sock == NULL)
		return ret;
	uint8_t buffer[100] = {0};
	simcom_t *simcom = sock->simcom;
	sprintf((char*)buffer, "+CIPSTART=\"TCP\",\"%s\",\"%d\"", host, port);
    do {
        if (simcom->proc->send_AT(simcom, (const char*)buffer) < 0)
            break;
        if (simcom->proc->receive_ex(simcom, "CONNECT OK", 6000, buffer, 100) < 0) {
            memset(buffer, 0, 100);
			sprintf((char*)buffer, "+CIPCLOSE=1");
            simcom->proc->send_AT(simcom, (const char*)buffer);
            simcom->proc->receive(simcom, buffer, 100, 1000);
            break;
        }
        sock->connected = true;
        ret = 0;
    } while (0);
    Logi("%s socket open %s", simcom->model, (ret == 0) ? FUNC_SUCCESS : FUNC_FAIL);
	return ret;
}
int socket_send(socket_t* socket, uint8_t* data, int size) {
	int reval = -1;
	uint8_t expect[5] = {0};
	int8_t ctrlz = 26;
	expect[0] = data[0];
	expect[1] = data[1];
	expect[2] = data[2];
	expect[3] = data[3];
	simcom_t* simcom = socket->simcom;
	do {
		uint8_t buffer[100] = {0};

		// sprintf((char*)buffer, "+CIPSEND=%d,%d", socket->index, size);
		sprintf((char*)buffer, "+CIPSEND=%d", size);

		if (simcom->proc->send_AT(simcom, (const char*)buffer) < 0)
			break;
 
        if (simcom->proc->receive_ex(simcom, ">", 3000, buffer, 100) < 0) {
            loge( "Socket %d connection not response char \">\"", socket->index);
			socket->error++;
			break;
        }
        simcom->pending = true;
		reval = -1;
		if (!simcom->serial->proc->write_data( data, size)) {
			Loge( "Socket %d connection write data failed", socket->index);
			break;
		}
		if (!simcom->serial->proc->write_data((uint8_t*)&ctrlz, 1)) { //send Ctrl+Z
			Loge( "Socket %d connection write Ctrl+Z failed", socket->index);
			break;
		}
        if (simcom->proc->receive_ex(simcom, (const char*)"DATA ACCEPT:", 3000, buffer, 100) < 0)
            break;
        reval = size;
	} while (0);
//	Logi("%s send socket %s", simcom->model, (reval > 0) ? FUNC_SUCCESS : FUNC_FAIL);
	return reval;
}

int socket_read(socket_t *socket, uint8_t *data, int size, unsigned int time_out)
{
	fifo_t * fifo = socket->fifo;
    uint32_t tick = eat_get_current_time();
    int index = 0;
	if (size == 1)
		return fifo->proc->fifoGetC(fifo, data);
	while (eat_get_current_time() - tick < time_out)
	{
		osDelay(10);
        index += fifo->proc->fifoGets(fifo, &data[index], size - 1 - index);
        if (index == size - 1)
			break;
	}
    return index;
}
int socket_close(socket_t* socket) {
	int reval = -1;
	uint8_t buffer[50] = {0};
	simcom_t* simcom = socket->simcom;
	// sprintf((char*)buffer, "+CIPCLOSE=%d,1", socket->index);
	sprintf((char*)buffer, "+CIPCLOSE=1");

	if ((simcom->proc->send_AT(simcom, (const char*)buffer) >= 0) && (simcom->proc->receive(simcom, buffer, 50, 1000) >= 0)) {
		if (strstr((const char*)buffer, "CLOSE OK") != NULL)
		{
			reval = 0;
			socket->connected = false;
		}

	}
	Logi("%s close socket %s", simcom->model, (reval == 0) ? FUNC_SUCCESS : FUNC_FAIL);
	return reval;
}

