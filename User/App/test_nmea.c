/*
 * test_nmea.c
 *
 *  Created on: Mar 17, 2023
 *      Author: ManhTran
 */
#include <string.h>
#include <stdio.h>
#include "lwgps.h"

lwgps_t hgps;

const char gps_rx_data[] = "$GPGGA,015540.000,3150.68378,N,11711.93139,E,1,17,0.6,0051.6,M,0.0,M,,*58\r\n";

void parser_nmea(void) {
    /* Init GPS */
    lwgps_init(&hgps);

    /* Process all input data */
    lwgps_process(&hgps, gps_rx_data, strlen(gps_rx_data));

    /* Print messages */
    printf("Latitude: %.ld degrees\r\n", (uint32_t)(hgps.latitude));
    printf("Longitude: %.ld degrees\r\n", (uint32_t)(hgps.longitude));
    printf("Time: %d:%d:%d \r\n", hgps.hours, hgps.minutes, hgps.seconds);
}

