/*
 * interrupt.c
 *
 *  Created on: Jan 11, 2023
 *      Author: manht
 */
#include <fifo.h>
#include "main.h"
#include "gps_type.h"
#include "string.h"
extern fifo_t *sc_rx;
extern uint8_t sc_data;

extern uint8_t gps_rx;

bool is_begin = false;
bool is_end = false;

gps_data_t data = {0};
int idx = 0;
static uint32_t lastTick = 0;
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart3)
	{
		FifoPutc(sc_rx, sc_data);
		HAL_UART_Receive_IT(&huart3, &sc_data, 1);
	}
	else if (huart == &huart4){
		if (gps_rx == '$' && !is_begin) {
			is_begin = true;
			memset(&data, 0, sizeof(gps_data_t));
			data.begin = HAL_GetTick() - lastTick;
			lastTick = HAL_GetTick();
			idx = 0;
		}
		else if (gps_rx == 10 && is_begin) {
			is_begin = false;
			is_end = true;
			data.size = idx;
		}
		if (is_begin) {
			data.data[idx++] = gps_rx;
		}
		HAL_UART_Receive_IT(&huart4, &gps_rx, 1);
	}
}

